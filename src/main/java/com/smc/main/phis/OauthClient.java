package com.smc.main.phis;

import java.net.URI;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.smc.main.model.MdpDprtMdl;

public class OauthClient {

	private static final Logger logger = LoggerFactory.getLogger(OauthClient.class);

	public String getOauth2Token(String username, String password) {

		final String CLIENT_ID = "OorkGC1GeL3FVvAvxeTfXulPoxEa";
		final String CLIENT_SECRET = "KjVFzfTRj9NsrgGYJW4GZtnhDR8a";
		final String GRANT_TYPE = "password";
		final String SERVER_URL = "http://210.89.179.113:8280";
		final String API_OAUTH_TOKEN = "/token";

		String clientCredentials = CLIENT_ID + ":" + CLIENT_SECRET;
		String base64ClientCredentials = new String(Base64.encodeBase64(clientCredentials.getBytes()));

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Authorization", "Basic " + base64ClientCredentials);

		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("grant_type", GRANT_TYPE);
		parameters.add("username", username);
		parameters.add("password", password);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(parameters, headers);

		@SuppressWarnings("rawtypes")
		ResponseEntity<Map> response;

		URI uri = URI.create(SERVER_URL + API_OAUTH_TOKEN);

		RestTemplate restTemplate = new RestTemplate();
		response = restTemplate.postForEntity(uri, request, Map.class);

		return (String) response.getBody().get("access_token");

	}
	
	public String getData(String URL) {

		String accessToken = this.getOauth2Token("fhiradmin", "7418bkyI5296");

		RestTemplate restTemplate = new RestTemplate();

		logger.debug("Access Token: " + accessToken);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + accessToken);
		headers.add("Accept", "*/*");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);

		ResponseEntity<String> responseEntity = restTemplate.exchange(URL, HttpMethod.GET, request,
				String.class);

		String reponseString = (String) responseEntity.getBody();
		logger.debug("Original Output : " + reponseString);

		return reponseString;

	}
	
	public String getData(String SERVER_URL, String API_URL) {

		String reponseString = this.getData(SERVER_URL + API_URL);

		return reponseString;

	}
}
