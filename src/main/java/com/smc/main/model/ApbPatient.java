package com.smc.main.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.hl7.fhir.r4.model.CodeType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.DateType;
import org.hl7.fhir.r4.model.Enumerations.DataType;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.codesystems.AdministrativeGender;

public class ApbPatient {

	public int ageVl;

	public String gendCd;

	public String ptno;

	public String ptntNm;

	public int getAgeVl() {
		return ageVl;
	}

	public void setAgeVl(int ageVl) {
		this.ageVl = ageVl;
	}

	public String getGendCd() {
		return gendCd;
	}

	public void setGendCd(String gendCd) {
		this.gendCd = gendCd;
	}

	public String getPtno() {
		return ptno;
	}

	public void setPtno(String ptno) {
		this.ptno = ptno;
	}

	public String getPtntNm() {
		return ptntNm;
	}

	public void setPtntNm(String ptntNm) {
		this.ptntNm = ptntNm;
	}

	public Patient generatePatientResource() {

		Patient patient = new Patient();

		// Set Id
		Coding code = new Coding();
		code.setCode("MR");
		code.setDisplay("Medical record number");
		code.setSystem("http://hl7.org/fhir/ValueSet/identifier-type");

		Identifier id = new Identifier();
		id.getType().addCoding(code);
		id.setValue(ptno);

		patient.getIdentifier().add(id);

		// Set Name
		HumanName humanName = new HumanName();
		humanName.setFamily(ptntNm.substring(0, 1));
		humanName.addGiven(ptntNm.substring(1));
		humanName.setText(ptntNm);
		patient.getName().add(humanName);

		if (gendCd.equals("M")) {
			patient.getGenderElement().setValueAsString("male");

		} else if (gendCd.equals("F")) {
			patient.getGenderElement().setValueAsString("female");
		}

		// 태어난 날짜 구하기
		int year = Calendar.getInstance().get(Calendar.YEAR);
		String birthYear = Integer.toString(year - ageVl + 1);
		DateType dataType = new DateType();
		dataType.fromStringValue(birthYear);
		patient.setBirthDateElement(dataType);

		return patient;
	}

}
