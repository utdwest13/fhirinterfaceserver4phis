package com.smc.main.model;

import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.r4.model.Observation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MedCnosrcrdBodyMeasurement {
	
	private static final Logger logger = LoggerFactory.getLogger(MedCnosrcrdBodyMeasurement.class);
	
	List<MedMsrnInfmOut> medMsrnInfmOutDVOList;

	public List<MedMsrnInfmOut> getMedMsrnInfmOutDVOList() {
		return medMsrnInfmOutDVOList;
	}

	public void setMedMsrnInfmOutDVOList(List<MedMsrnInfmOut> medMsrnInfmOutDVOList) {
		this.medMsrnInfmOutDVOList = medMsrnInfmOutDVOList;
	}
	
	public List<Observation> generateObservationREsourceBundle(String patientId, String encounterId) {
		
		logger.info("Call MedCnosrcrdBodyMeasurement.generateObservationREsourceBundle()");
		
		List<Observation> observationList = new ArrayList<Observation>();
		
		String logicalIdPrefix = "OB"+patientId+"-20190926-MMI-";
		
		for (MedMsrnInfmOut medMsrnInfmOut : medMsrnInfmOutDVOList) {
			
			List<Observation> observationSubList = medMsrnInfmOut.generateObservationResourceList(logicalIdPrefix, patientId, encounterId);
			
			observationList.addAll(observationSubList);
			
		}
			
		return observationList;
		
	}

}
