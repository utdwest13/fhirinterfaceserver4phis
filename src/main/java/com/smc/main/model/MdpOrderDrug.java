package com.smc.main.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.r4.model.MedicationRequest;

public class MdpOrderDrug {

	List<MdpOrderInfoDrugOut> mdpOrdrInfoDrugOutDVOList;

	public List<MedicationRequest> generateMedicationResourceBundle() {

		String logicalIdPrefix = "MR10021900-20190926-MOD-";

		List<MedicationRequest> retVal = new ArrayList<MedicationRequest>();

		int counter = 0;
		for (MdpOrderInfoDrugOut mdpOrderInfoDrugOut : mdpOrdrInfoDrugOutDVOList) {

			retVal.add(mdpOrderInfoDrugOut.generateMedicationRequestResource(logicalIdPrefix + counter, "10021900"));
			counter++;

		}

		return retVal;
	}

	public List<MedicationRequest> generateMedicationResourceBundle(String patientId, Date orderDate) {

		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		String date = format.format(orderDate);

		String logicalIdPrefix = "MR" + patientId + "-" + date + "-MOD-";

		List<MedicationRequest> retVal = new ArrayList<MedicationRequest>();

		int counter = 0;
		for (MdpOrderInfoDrugOut mdpOrderInfoDrugOut : mdpOrdrInfoDrugOutDVOList) {

			retVal.add(mdpOrderInfoDrugOut.generateMedicationRequestResource(logicalIdPrefix + counter, patientId,
					orderDate));
			counter++;

		}

		return retVal;
	}

}
