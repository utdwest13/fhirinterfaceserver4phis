package com.smc.main.model;

import java.util.List;

public class MdpDprtMdl {
	
	List<MdpDprtInfmOut> mdpDprtInfmOutDVOList;

	public List<MdpDprtInfmOut> getMdpDprtInfmOutDVOList() {
		return mdpDprtInfmOutDVOList;
	}

	public void setMdpDprtInfmOutDVOList(List<MdpDprtInfmOut> mdpDprtInfmOutDVOList) {
		this.mdpDprtInfmOutDVOList = mdpDprtInfmOutDVOList;
	}

}
