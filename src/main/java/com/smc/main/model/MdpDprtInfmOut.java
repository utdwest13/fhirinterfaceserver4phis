package com.smc.main.model;

public class MdpDprtInfmOut {

	public String abrvDprtCd;

	public String dprtCd;

	public String kornDprtNm;

	public String lcdvNm;

	public String getAbrvDprtCd() {
		return abrvDprtCd;
	}

	public void setAbrvDprtCd(String abrvDprtCd) {
		this.abrvDprtCd = abrvDprtCd;
	}

	public String getDprtCd() {
		return dprtCd;
	}

	public void setDprtCd(String dprtCd) {
		this.dprtCd = dprtCd;
	}

	public String getKornDprtNm() {
		return kornDprtNm;
	}

	public void setKornDprtNm(String kornDprtNm) {
		this.kornDprtNm = kornDprtNm;
	}

	public String getLcdvNm() {
		return lcdvNm;
	}

	public void setLcdvNm(String lcdvNm) {
		this.lcdvNm = lcdvNm;
	}

}
