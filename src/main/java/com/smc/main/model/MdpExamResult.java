package com.smc.main.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hl7.fhir.r4.model.Observation;


public class MdpExamResult {

	List<MdpExamRsltOut> mdpExmnRsltOutDVOList;

	public List<MdpExamRsltOut> getMdpExmnRsltOutDVOList() {
		return mdpExmnRsltOutDVOList;
	}

	public void setMdpExmnRsltOutDVOList(List<MdpExamRsltOut> mdpExmnRsltOutDVOList) {
		this.mdpExmnRsltOutDVOList = mdpExmnRsltOutDVOList;
	}
	
	public List<Observation> generateObservationResourceBundle() {
		
		List<Observation> retVal = new ArrayList<Observation>();
		
		String logicalIdPrefix = "OB10021900-20190926-MER-";
		int counter = 0;
		
		for (MdpExamRsltOut mdpExamRsltOut : mdpExmnRsltOutDVOList) {
			
			retVal.add(mdpExamRsltOut.generateObservationResource(logicalIdPrefix+counter, "10021900"));
			counter++;
		}
		
		return retVal;
	}
	
	public List<Observation> generateObservationResourceBundle(String patientId) {
		
		List<Observation> retVal = new ArrayList<Observation>();
		
		//Counter Observations operated at same date
		Map<String, Integer> counterMap = new HashMap<String, Integer>();		
				
		for (MdpExamRsltOut mdpExamRsltOut : mdpExmnRsltOutDVOList) {
			
			String date = mdpExamRsltOut.getEnfrDt();
			
			int counter = 0;
			
			if (counterMap.containsKey(date) ) {
				counter = counterMap.get(date).intValue();
				counter++;
			} 
			
			counterMap.put(date, new Integer(counter));	
			
			String logicalIdPrefix = "OB" + patientId + "-" + date + "-MER-";
						
			retVal.add(mdpExamRsltOut.generateObservationResource(logicalIdPrefix+counter, patientId));
		}
		
		return retVal;
	}
	
	
}
