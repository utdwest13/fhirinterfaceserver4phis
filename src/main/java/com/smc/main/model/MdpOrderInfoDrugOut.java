package com.smc.main.model;

import java.util.Date;

import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.MedicationRequest;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestIntent;
import org.hl7.fhir.r4.model.MedicationRequest.MedicationRequestStatus;
import org.hl7.fhir.r4.model.Timing.UnitsOfTime;

public class MdpOrderInfoDrugOut {

	public float afiDrugAdqtStr;

	public String ddcn;

	public String detlCdNm;

	public String drugOrdrPrssCd;

	public String drusCd;

	public String mdtnUnitCtn;

	public String ntm;

	public String ordrCd;

	public String ordrNm;

	public String orfrNm;

	public float getAfiDrugAdqtStr() {
		return afiDrugAdqtStr;
	}

	public void setAfiDrugAdqtStr(float afiDrugAdqtStr) {
		this.afiDrugAdqtStr = afiDrugAdqtStr;
	}

	public String getDdcn() {
		return ddcn;
	}

	public void setDdcn(String ddcn) {
		this.ddcn = ddcn;
	}

	public String getDetlCdNm() {
		return detlCdNm;
	}

	public void setDetlCdNm(String detlCdNm) {
		this.detlCdNm = detlCdNm;
	}

	public String getDrugOrdrPrssCd() {
		return drugOrdrPrssCd;
	}

	public void setDrugOrdrPrssCd(String drugOrdrPrssCd) {
		this.drugOrdrPrssCd = drugOrdrPrssCd;
	}

	public String getDrusCd() {
		return drusCd;
	}

	public void setDrusCd(String drusCd) {
		this.drusCd = drusCd;
	}

	public String getMdtnUnitCtn() {
		return mdtnUnitCtn;
	}

	public void setMdtnUnitCtn(String mdtnUnitCtn) {
		this.mdtnUnitCtn = mdtnUnitCtn;
	}

	public String getNtm() {
		return ntm;
	}

	public void setNtm(String ntm) {
		this.ntm = ntm;
	}

	public String getOrdrCd() {
		return ordrCd;
	}

	public void setOrdrCd(String ordrCd) {
		this.ordrCd = ordrCd;
	}

	public String getOrdrNm() {
		return ordrNm;
	}

	public void setOrdrNm(String ordrNm) {
		this.ordrNm = ordrNm;
	}

	public String getOrfrNm() {
		return orfrNm;
	}

	public void setOrfrNm(String orfrNm) {
		this.orfrNm = orfrNm;
	}

	public MedicationRequest generateMedicationRequestResource() {

		MedicationRequest medicationRequest = new MedicationRequest();

		String logicalId = "MR10021900-20190926";
		String versionId = "1"; // optional
		medicationRequest.setId(new IdType("MedicationRequest", logicalId, versionId));

		medicationRequest.setStatus(MedicationRequestStatus.COMPLETED);
		medicationRequest.setIntent(MedicationRequestIntent.ORDER);
		medicationRequest.getSubject().setReference("Patient/02099880");

		return medicationRequest;

	}

	public MedicationRequest generateMedicationRequestResource(String logicalId, String patientLogicalId
		) {

		MedicationRequest medicationRequest = new MedicationRequest();

		String versionId = "1"; // optional
		medicationRequest.setId(new IdType("MedicationRequest", logicalId, versionId));

		medicationRequest.setStatus(MedicationRequestStatus.COMPLETED);
		medicationRequest.setIntent(MedicationRequestIntent.ORDER);
		medicationRequest.getSubject().setReference("Patient/" + patientLogicalId);


		// Set 약 정보
		medicationRequest.getMedicationCodeableConcept().getCodingFirstRep().setCode(this.ordrCd);
		medicationRequest.getMedicationCodeableConcept().getCodingFirstRep().setDisplay(this.ordrNm);
		medicationRequest.getMedicationCodeableConcept().getCodingFirstRep().setUserSelected(true);

		// Set 1일 투약 횟수
		medicationRequest.getDosageInstructionFirstRep().getTiming().getRepeat()
				.setFrequency(Integer.parseInt(this.ntm.trim()));
		medicationRequest.getDosageInstructionFirstRep().getTiming().getRepeat().setPeriod(1);
		medicationRequest.getDosageInstructionFirstRep().getTiming().getRepeat().setPeriodUnit(UnitsOfTime.D);
		
		// Set 투약 용법
		medicationRequest.getDosageInstructionFirstRep().setText(drusCdtoText(drusCd));
		
		// Set 투약 일수
		if (ddcn != null) {
			medicationRequest.getDispenseRequest().getExpectedSupplyDuration()
					.setValue(Integer.parseInt(this.ddcn.trim()));
			medicationRequest.getDispenseRequest().getExpectedSupplyDuration().setCode("d");
			medicationRequest.getDispenseRequest().getExpectedSupplyDuration().setUnit("days");
			medicationRequest.getDispenseRequest().getExpectedSupplyDuration().setSystem("http://unitsofmeasure.org");
		}

		return medicationRequest;

	}

	public MedicationRequest generateMedicationRequestResource(String logicalId, String patientLogicalId,
			Date orderDate) {
				
		MedicationRequest medicationRequest = this.generateMedicationRequestResource(logicalId, patientLogicalId);
		medicationRequest.setAuthoredOn(orderDate);		
		return medicationRequest;

	}
	
	private String drusCdtoText(String drusCd) {
		
		String retVal = null;
		
		switch (drusCd) { 
			case "M" : 
				retVal ="1일 1회 아침식후 30분에 복용" ;
				break;
			case "3P" : 
				retVal ="1일 3회 실후 30분에 복용" ;
				break;
			case "MHS" : 
				retVal ="1일 2회 아침식후 30분, 취침전에 복용" ;
				break;
			case "HS" : 
				retVal ="1일 1회 취침전에 복용" ;
				break;
			case "AM1" : 
				retVal ="1일 1회 아침식전 1시간에 복용" ;
				break;
			case "INF" : 
				retVal ="정맥내 점적 주입" ;
				break;	
		}
		
		return retVal; 
		
	}

}
