package com.smc.main.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Observation.ObservationStatus;

public class MdpExamRsltOut {

	public String enfrDt;

	public String exmnCd;

	public String exmnNm;

	public String exmnPrssCd;

	public String exmnPrssNm;

	public String exrmRptnDt;

	public String exrsCtn;
	
	public String exrsFrmtCd;

	public String exrsNcvlVl;

	public String exrsUnitNm;

	public String indrNm1;

	public String indrNm2;

	public String indrNm3;

	public String inrfVn;

	public String intrDt;

	public String lwlmUplmDvsnVl;

	public String nrmlLwlmNcvlVl;

	public String nrmlUplmNcvlVl;

	public String ordrCd;

	public int ordrSno;

	public String ordrYmd;

	public String pacsAccsNo;

	public String ptno;

	public String rprtDt;

	public String slipCd;

	public String slipNm;

	public String spcmNo;

	public String upafExrsFrmtCd;

	public String getEnfrDt() {
		return enfrDt;
	}





	public void setEnfrDt(String enfrDt) {
		this.enfrDt = enfrDt;
	}





	public String getExmnCd() {
		return exmnCd;
	}





	public void setExmnCd(String exmnCd) {
		this.exmnCd = exmnCd;
	}





	public String getExmnNm() {
		return exmnNm;
	}





	public void setExmnNm(String exmnNm) {
		this.exmnNm = exmnNm;
	}





	public String getExmnPrssCd() {
		return exmnPrssCd;
	}





	public void setExmnPrssCd(String exmnPrssCd) {
		this.exmnPrssCd = exmnPrssCd;
	}





	public String getExmnPrssNm() {
		return exmnPrssNm;
	}





	public void setExmnPrssNm(String exmnPrssNm) {
		this.exmnPrssNm = exmnPrssNm;
	}





	public String getExrmRptnDt() {
		return exrmRptnDt;
	}





	public void setExrmRptnDt(String exrmRptnDt) {
		this.exrmRptnDt = exrmRptnDt;
	}





	public String getExrsCtn() {
		return exrsCtn;
	}





	public void setExrsCtn(String exrsCtn) {
		this.exrsCtn = exrsCtn;
	}





	public String getExrsFrmtCd() {
		return exrsFrmtCd;
	}





	public void setExrsFrmtCd(String exrsFrmtCd) {
		this.exrsFrmtCd = exrsFrmtCd;
	}





	public String getExrsNcvlVl() {
		return exrsNcvlVl;
	}





	public void setExrsNcvlVl(String exrsNcvlVl) {
		this.exrsNcvlVl = exrsNcvlVl;
	}





	public String getExrsUnitNm() {
		return exrsUnitNm;
	}





	public void setExrsUnitNm(String exrsUnitNm) {
		this.exrsUnitNm = exrsUnitNm;
	}





	public String getIndrNm1() {
		return indrNm1;
	}





	public void setIndrNm1(String indrNm1) {
		this.indrNm1 = indrNm1;
	}





	public String getIndrNm2() {
		return indrNm2;
	}





	public void setIndrNm2(String indrNm2) {
		this.indrNm2 = indrNm2;
	}





	public String getIndrNm3() {
		return indrNm3;
	}





	public void setIndrNm3(String indrNm3) {
		this.indrNm3 = indrNm3;
	}





	public String getInrfVn() {
		return inrfVn;
	}





	public void setInrfVn(String inrfVn) {
		this.inrfVn = inrfVn;
	}





	public String getIntrDt() {
		return intrDt;
	}





	public void setIntrDt(String intrDt) {
		this.intrDt = intrDt;
	}


	public String getLwlmUplmDvsnVl() {
		return lwlmUplmDvsnVl;
	}

	public void setLwlmUplmDvsnVl(String lwlmUplmDvsnVl) {
		this.lwlmUplmDvsnVl = lwlmUplmDvsnVl;
	}

	public String getNrmlLwlmNcvlVl() {
		return nrmlLwlmNcvlVl;
	}

	public void setNrmlLwlmNcvlVl(String nrmlLwlmNcvlVl) {
		this.nrmlLwlmNcvlVl = nrmlLwlmNcvlVl;
	}

	public String getNrmlUplmNcvlVl() {
		return nrmlUplmNcvlVl;
	}

	public void setNrmlUplmNcvlVl(String nrmlUplmNcvlVl) {
		this.nrmlUplmNcvlVl = nrmlUplmNcvlVl;
	}

	public String getOrdrCd() {
		return ordrCd;
	}

	public void setOrdrCd(String ordrCd) {
		this.ordrCd = ordrCd;
	}

	public int getOrdrSno() {
		return ordrSno;
	}

	public void setOrdrSno(int ordrSno) {
		this.ordrSno = ordrSno;
	}

	public String getOrdrYmd() {
		return ordrYmd;
	}

	public void setOrdrYmd(String ordrYmd) {
		this.ordrYmd = ordrYmd;
	}


	public String getPacsAccsNo() {
		return pacsAccsNo;
	}

	public void setPacsAccsNo(String pacsAccsNo) {
		this.pacsAccsNo = pacsAccsNo;
	}

	public String getPtno() {
		return ptno;
	}

	public void setPtno(String ptno) {
		this.ptno = ptno;
	}

	public String getRprtDt() {
		return rprtDt;
	}

	public void setRprtDt(String rprtDt) {
		this.rprtDt = rprtDt;
	}

	public String getSlipCd() {
		return slipCd;
	}

	public void setSlipCd(String slipCd) {
		this.slipCd = slipCd;
	}

	public String getSlipNm() {
		return slipNm;
	}

	public void setSlipNm(String slipNm) {
		this.slipNm = slipNm;
	}

	public String getSpcmNo() {
		return spcmNo;
	}

	public void setSpcmNo(String spcmNo) {
		this.spcmNo = spcmNo;
	}

	public String getUpafExrsFrmtCd() {
		return upafExrsFrmtCd;
	}

	public void setUpafExrsFrmtCd(String upafExrsFrmtCd) {
		this.upafExrsFrmtCd = upafExrsFrmtCd;
	}

	public Observation generateObservationResource(String logicalId, String patientLogicalId) {

		Observation observation = new Observation();

		String versionId = "1"; // optional
		observation.setId(new IdType("Observation", logicalId, versionId));

		// Set 검사 진행 상태 정보
		observation.setStatus(ObservationStatus.FINAL);

		// Set 검사유형
		observation.getCategoryFirstRep().getCodingFirstRep().setCode("laboratory");
		observation.getCategoryFirstRep().getCodingFirstRep().setDisplay("Laboratory");
		observation.getCategoryFirstRep().getCodingFirstRep().setSystem("http://terminology.hl7.org/CodeSystem/observation-category");

		// Set 검사코드
		observation.getCode().getCodingFirstRep().setCode(this.exmnCd);
		observation.getCode().getCodingFirstRep().setDisplay(this.exmnNm);
		observation.getCode().setText(this.exmnNm);
		observation.getCode().getCodingFirstRep().setUserSelected(true);

		// Set Patient Resource
		observation.getSubject().setReference("Patient/" + patientLogicalId);

		// Set effectiveTime
		try {
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date effectiveDateTime = transFormat.parse(this.enfrDt);
			observation.getEffectiveDateTimeType().setValue(effectiveDateTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Set Value
		if (this.exrsFrmtCd.equals("N") || this.exrsFrmtCd.equals("C")) {
			
			// Check whether this.exrsNcvlVl is Nummeric
			if ( isNumeric(this.exrsNcvlVl) ) {
				observation.getValueQuantity().setValue(Double.parseDouble(this.exrsNcvlVl));
				
				if (this.exrsUnitNm != null) {
					observation.getValueQuantity().setUnit(this.exrsUnitNm);
				}
				
			} else {
				
				if (this.exrsUnitNm != null) {
					observation.getValueStringType().setValue(this.exrsNcvlVl + " " + this.exrsUnitNm);
				} else {
					observation.getValueStringType().setValue(this.exrsNcvlVl);
				}						
			}
			
			
			//Set ReferenceRange
			if (this.nrmlLwlmNcvlVl != null) {
				
				if ( isNumeric(this.nrmlLwlmNcvlVl) ) {
					observation.getReferenceRangeFirstRep().getLow().setValue(Double.parseDouble(this.nrmlLwlmNcvlVl));
				}
				observation.getReferenceRangeFirstRep().setText(this.nrmlLwlmNcvlVl);
			}			
			if (this.nrmlUplmNcvlVl != null) {
				if ( isNumeric(this.nrmlLwlmNcvlVl) ) {
					observation.getReferenceRangeFirstRep().getHigh().setValue(Double.parseDouble(this.nrmlUplmNcvlVl));
				}				
			}											
		} 
		

		return observation;
	}
	
	private static boolean isNumeric(String s) {
		  try {
		      Double.parseDouble(s);
		      return true;
		  } catch(NumberFormatException e) {
		      return false;
		  }
		}
	
	

}
