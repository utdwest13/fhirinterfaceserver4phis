package com.smc.main.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Observation.ObservationStatus;

public class MedMsrnInfmOut {
			
	public String cdVl;

	public String cdVl1;
	
	public String cdVl2;

	public String getCdVl() {
		return cdVl;
	}

	public void setCdVl(String cdVl) {
		this.cdVl = cdVl;
	}

	public String getCdVl1() {
		return cdVl1;
	}

	public void setCdVl1(String cdVl1) {
		this.cdVl1 = cdVl1;
	}

	public String getCdVl2() {
		return cdVl2;
	}

	public void setCdVl2(String cdVl2) {
		this.cdVl2 = cdVl2;
	}
	
	public List<Observation> generateObservationResourceList(String logicalId, String patientLogicalId, String encounterLogicalId) {
		
		List<Observation> observationList = new ArrayList<Observation>();
		
		int counter = 0;
		
		//Generate observation Resource for weight
		counter++;
		if (cdVl1 != null) {
			
			Observation observation = this.generateObservationResource(logicalId+counter, patientLogicalId);
			
			
			// Set 계측코드
			observation.getCode().getCodingFirstRep().setCode("3141-9");
			observation.getCode().getCodingFirstRep().setDisplay("weight");
			observation.getCode().setText("체중");
			
			//Set Encounter
			observation.getEncounter().setReference("Encounter/"+encounterLogicalId);
			
			//Set 계측결과
			observation.getValueQuantity().setValue(Double.parseDouble(cdVl1));
			observation.getValueQuantity().setUnit("kg");
			observation.getValueQuantity().setCode("kg");
			
			observationList.add(observation);
		}
		
		
		//Generate Observation Resource for height
		counter++;
		if (cdVl2 != null) {
			
			Observation observation = this.generateObservationResource(logicalId+counter, patientLogicalId);
			
			// Set 계측코드
			observation.getCode().getCodingFirstRep().setCode("8302-2");
			observation.getCode().getCodingFirstRep().setDisplay("height");
			observation.getCode().setText("신장");
			
			//Set Encounter
			observation.getEncounter().setReference("Encounter/"+encounterLogicalId);
			
			//Set 계측결과
			observation.getValueQuantity().setValue(Double.parseDouble(cdVl2));
			observation.getValueQuantity().setUnit("cm");
			observation.getValueQuantity().setCode("cm");
			
			observationList.add(observation);
		}
			
		//Generate Observation Resource for BMI
		counter++;
		if (cdVl != null) {
			
			Observation observation = this.generateObservationResource(logicalId+counter, patientLogicalId);
			
			// Set 계측코드
			observation.getCode().getCodingFirstRep().setCode("39156-5");
			observation.getCode().getCodingFirstRep().setDisplay("BMI");
			observation.getCode().setText("BMI");
			
			//Set Encounter
			observation.getEncounter().setReference("Encounter/"+encounterLogicalId);
			
			//Set 계측결과
			observation.getValueQuantity().setValue(Double.parseDouble(cdVl));
			observation.getValueQuantity().setUnit("kg/m2");
			observation.getValueQuantity().setCode("kg/m2");
			
			observationList.add(observation);		
		}
		
		
		return observationList;
		
	}
	
	private Observation generateObservationResource(String logicalId, String patientLogicalId) {
	
		Observation observation = new Observation();
		
		String versionId = "1"; // optional
		observation.setId(new IdType("Observation", logicalId, versionId));
		
		// Set 검사 진행 상태 정보
		observation.setStatus(ObservationStatus.FINAL);
		
		// Set 검사유형
		observation.getCategoryFirstRep().getCodingFirstRep().setCode("vital-signs");
		observation.getCategoryFirstRep().getCodingFirstRep().setDisplay("Vital Signs");
		observation.getCategoryFirstRep().getCodingFirstRep().setSystem("http://terminology.hl7.org/CodeSystem/observation-category");
		
		// Set 계측코드 System
		observation.getCode().getCodingFirstRep().setSystem("http://loinc.org");

		
		// Set Patient Resource
		observation.getSubject().setReference("Patient/" + patientLogicalId);
		
		// Set Value Unit System
		observation.getValueQuantity().setSystem("http://unitsofmeasure.org");
		
		
		// Set effectiveDateTime
		try {
			SimpleDateFormat transFormat = new SimpleDateFormat("yyyyMMdd");
			Date effectiveDateTime = transFormat.parse("20190926");
			observation.getEffectiveDateTimeType().setValue(effectiveDateTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return observation;
		
	}
	
}
