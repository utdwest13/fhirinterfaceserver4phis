package com.smc.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FhirInterfaceServer4PhisApplication {

	public static void main(String[] args) {
		SpringApplication.run(FhirInterfaceServer4PhisApplication.class, args);
	}

}


