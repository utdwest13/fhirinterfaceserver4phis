package com.smc.main.provider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hl7.fhir.r4.model.Observation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.smc.main.model.MdpExamResult;
import com.smc.main.model.MedCnosrcrdBodyMeasurement;
import com.smc.main.phis.OauthClient;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jaxrs.server.AbstractJaxRsResourceProvider;
import ca.uhn.fhir.rest.annotation.Count;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.param.TokenOrListParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;

@Component
public class ObservationResourceProvider extends AbstractJaxRsResourceProvider<Observation> {

	private static final Logger logger = LoggerFactory.getLogger(ObservationResourceProvider.class);

	private static Long counter = 1L;

	private static String SERVER_URL = "http://210.89.179.113:8280/ehr-mbl/1.0";

	public ObservationResourceProvider(FhirContext fhirContext) {
		super(fhirContext);
	}

	@Override
	public Class<Observation> getResourceType() {
		// TODO Auto-generated method stub
		return Observation.class;
	}

	@Search
	public List<Observation> findObservation(@OptionalParam(name = Observation.SP_SUBJECT) ReferenceParam theSubject,
			@OptionalParam(name = Observation.SP_DATE) DateRangeParam theRange,
			@OptionalParam(name = Observation.SP_ENCOUNTER) ReferenceParam theEncounter,
			@OptionalParam(name = Observation.SP_CATEGORY) TokenParam theCategory,
			@OptionalParam(name = Observation.SP_CODE) TokenOrListParam theCodes,
			@OptionalParam(name = Observation.SP_PATIENT) ReferenceParam thePatient, 
			@Count Integer theCount
			) {

		List<Observation> retVal = new ArrayList<Observation>();

		
		
		//Set parameter variable
		String subjectId = null;  
		
		String encounterId = null;
		
		String categoryCode = null;
		
		String startYmd = null;
		String endYmd = null;
		
		// Set Date
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

		if (theRange != null) {			
			Date from = theRange.getLowerBoundAsInstant();
			Date to = theRange.getUpperBoundAsInstant();

			if ( from != null ) {
				startYmd = format.format(from);				
			} else {
				startYmd = "20000101";
			}
			
			if ( to != null ) {
				endYmd = format.format(to);
			} else {
				Date time = new Date();				
				endYmd = format.format(time);
			}
		} else {
			startYmd = "20000101";
			
			Date time = new Date();				
			endYmd = format.format(time);
		}
		
		logger.debug("strt-ymd: " + startYmd + ", fnsh-ymd:" + endYmd);
		
		
		
		//Set default encounterId
		if (theEncounter != null) {
			encounterId = theEncounter.getValue();
		} else {
			encounterId = "1029091";
		}				
		
		logger.debug("Encounter Id: " + encounterId);
		

		// Set Logic for Search 
		if (theSubject != null ) {
			
			if (theSubject.hasResourceType()) {
				
				String resourceType = theSubject.getResourceType();
				if ("Patient".equals(resourceType) == false) {
					throw new InvalidRequestException("Invalid resource type for parameter 'subject': " + resourceType);
				}
			}

			subjectId = theSubject.getIdPart();
			
			logger.debug("Subject Id: " + subjectId);

			//Get Category Code
			if (theCategory != null) {
				categoryCode = theCategory.getValue();
//				String categorySystem = theCategory.getSystem();
				
				logger.debug("Params Category Code : " + categoryCode);
//				logger.debug("Params Category Code : " + categoryCode + ", Category System :" +categorySystem );
				
				//Get ExamResult
				if (categoryCode.equals("laboratory")) {
					retVal.addAll(this.getExmnResult(subjectId, startYmd, endYmd));
					
				//Get BodyMeasurement
				} else if (categoryCode.equals("vital-signs") ) {								
					retVal.addAll(this.getBodyMeasurement(subjectId, encounterId) );
				}
				
				//Get BodyMeasurement for Gross Chart
			} else if (theCodes != null) {
			
				List<TokenParam> wantedCodes =  theCodes.getValuesAsQueryTokens();
				
				for(TokenParam codeParam : wantedCodes) {
					String code = codeParam.getValue();
					
					logger.debug("Search Parameter Code : " + code );
				}
				
				retVal.addAll(this.getBodyMeasurement(subjectId, encounterId) );
				
			} else { 
				//Get all Observation by patient id 
				retVal.addAll(this.getExmnResult(subjectId, startYmd, endYmd));
				retVal.addAll(this.getBodyMeasurement(subjectId, encounterId) );
			}

		} else if (theEncounter != null || thePatient != null) {
		
			subjectId = "10021900";
			
			retVal.addAll(this.getBodyMeasurement(subjectId, encounterId));
		} 

		return retVal;

	}
	
	private List<Observation> getExmnResult(String ptno, String strtYmd, String endYmd) {
		
		List<Observation> observationResult = new ArrayList<Observation>();
		
		String responseString = null;
		
		OauthClient oauthClient = new OauthClient();

		String API_URL = "/api/v1/mdp/patients/"+ptno+"/orders/exam/patient-exam-result";

		// Set Parameters
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(SERVER_URL + API_URL)
				.queryParam("strt-ymd", strtYmd)							
				.queryParam("fnsh-ymd", endYmd)
				.queryParam("dvsn-cd", "1");
		

		responseString = oauthClient.getData(builder.toUriString());

//		logger.debug("JSON Output: " + responseString);
		
		// Set Sample data for Test
//		responseString = "{\r\n" + "  \"mdpExmnRsltOutDVOList\": [\r\n" + "    {\r\n"
//				+ "      \"ptno\": \"10021900\",\r\n" + "      \"ordrYmd\": \"20181206\",\r\n"
//				+ "      \"ordrSno\": 445069,\r\n" + "      \"ordrCd\": \"BL2000G1\",\r\n"
//				+ "      \"exmnCd\": \"BL2001\",\r\n" + "      \"exmnNm\": \"WBC Count\",\r\n"
//				+ "      \"slipCd\": \"L20\",\r\n" + "      \"slipNm\": \"일반혈액\",\r\n"
//				+ "      \"exrsFrmtCd\": \"N\",\r\n" + "      \"exrsNcvlVl\": \"4.4\",\r\n"
//				+ "      \"exrsUnitNm\": \"x10³/㎕\",\r\n" + "      \"nrmlLwlmNcvlVl\": \"4.3\",\r\n"
//				+ "      \"nrmlUplmNcvlVl\": \"9.4\",\r\n" + "      \"exmnPrssCd\": \"G\",\r\n"
//				+ "      \"exmnPrssNm\": \"확인/보고\",\r\n" + "      \"exrmRptnDt\": \"20181206141334\",\r\n"
//				+ "      \"enfrDt\": \"20181206141334\",\r\n" + "      \"intrDt\": null,\r\n"
//				+ "      \"rprtDt\": \"20181206142635\",\r\n" + "      \"spcmNo\": \"1820007280\",\r\n"
//				+ "      \"pacsAccsNo\": null,\r\n" + "      \"inrfYn\": \"N\",\r\n"
//				+ "      \"upafExrsFrmtCd\": \"N\",\r\n" + "      \"lwlmUplmDvsnVl\": null,\r\n"
//				+ "      \"exrsCtn\": null,\r\n" + "      \"indrNm1\": null,\r\n" + "      \"indrNm2\": null,\r\n"
//				+ "      \"indrNm3\": null\r\n" + "    },\r\n" + "    {\r\n" + "      \"ptno\": \"10021900\",\r\n"
//				+ "      \"ordrYmd\": \"20181206\",\r\n" + "      \"ordrSno\": 445069,\r\n"
//				+ "      \"ordrCd\": \"BL2000G1\",\r\n" + "      \"exmnCd\": \"BL2002\",\r\n"
//				+ "      \"exmnNm\": \"RBC Count\",\r\n" + "      \"slipCd\": \"L20\",\r\n"
//				+ "      \"slipNm\": \"일반혈액\",\r\n" + "      \"exrsFrmtCd\": \"N\",\r\n"
//				+ "      \"exrsNcvlVl\": \"3.11\",\r\n" + "      \"exrsUnitNm\": \"x100³/㎕\",\r\n"
//				+ "      \"nrmlLwlmNcvlVl\": \"3.81\",\r\n" + "      \"nrmlUplmNcvlVl\": \"4.69\",\r\n"
//				+ "      \"exmnPrssCd\": \"G\",\r\n" + "      \"exmnPrssNm\": \"확인/보고\",\r\n"
//				+ "      \"exrmRptnDt\": \"20181206141334\",\r\n" + "      \"enfrDt\": \"20181206141334\",\r\n"
//				+ "      \"intrDt\": null,\r\n" + "      \"rprtDt\": \"20181206142635\",\r\n"
//				+ "      \"spcmNo\": \"1820007280\",\r\n" + "      \"pacsAccsNo\": null,\r\n"
//				+ "      \"inrfYn\": \"N\",\r\n" + "      \"upafExrsFrmtCd\": \"N\",\r\n"
//				+ "      \"lwlmUplmDvsnVl\": \"LL\",\r\n" + "      \"exrsCtn\": null,\r\n"
//				+ "      \"indrNm1\": null,\r\n" + "      \"indrNm2\": null,\r\n" + "      \"indrNm3\": null\r\n"
//				+ "  }\r\n" + "  ]\r\n" + "}";

		MdpExamResult mdpExamResult = new Gson().fromJson(responseString, MdpExamResult.class);

		observationResult.addAll(mdpExamResult.generateObservationResourceBundle());
		
		return observationResult;
		
	}
	
	private List<Observation> getBodyMeasurement(String patientId, String  encounterId) {
		
		List<Observation> observationResult = new ArrayList<Observation>();

		String responseString = null;

		OauthClient oauthClient = new OauthClient();

		String API_URL = "/api/v1/med/cnosrcrd/"+ encounterId +"/pateint-body-measurement-info";

		responseString = oauthClient.getData(SERVER_URL, API_URL);

//		logger.debug("JSON Output: " + responseString);

//		responseString = "{\r\n" + "  \"medMsrnInfmOutDVOList\": [\r\n" + "    {\r\n"
//				+ "      \"cdVl1\": \"65\",\r\n" + "      \"cdVl2\": \"154.7\",\r\n"
//				+ "      \"cdVl\": \"27.2\"\r\n" + "    }\r\n" + "  ]\r\n" + "}";

		MedCnosrcrdBodyMeasurement medCnosrcrdBodyMeasurement = new Gson().fromJson(responseString,
				MedCnosrcrdBodyMeasurement.class);

		observationResult.addAll(medCnosrcrdBodyMeasurement.generateObservationREsourceBundle(patientId ,encounterId));
		
		return observationResult;
		
	}

}
