package com.smc.main.provider;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.MedicationRequest;
import org.hl7.fhir.r4.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.smc.main.model.ApbPatient;
import com.smc.main.model.MdpOrderDrug;
import com.smc.main.phis.OauthClient;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jaxrs.server.AbstractJaxRsResourceProvider;
import ca.uhn.fhir.rest.annotation.Delete;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateParam;
import ca.uhn.fhir.rest.param.ReferenceParam;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;

@Component
public class MedicationRequestResourceProvider extends AbstractJaxRsResourceProvider<MedicationRequest> {

	private static final Logger logger = LoggerFactory.getLogger(MedicationRequestResourceProvider.class);

	private static Long counter = 1L;

	private static String SERVER_URL = "http://210.89.179.113:8280/ehr-mbl/1.0";

	public MedicationRequestResourceProvider(FhirContext fhirContext) {
		super(fhirContext);
	}

	@Override
	public Class<MedicationRequest> getResourceType() {
		// TODO Auto-generated method stub
		return MedicationRequest.class;
	}

	@Search
	public List<MedicationRequest> findMedicationRequestWithSubject(
			@OptionalParam(name = MedicationRequest.SP_SUBJECT) ReferenceParam theSubject,
			@OptionalParam(name = MedicationRequest.SP_AUTHOREDON) DateParam theDate) {

		List<MedicationRequest> retVal = null;

		if (theSubject.hasResourceType()) {

			String resourceType = theSubject.getResourceType();
			if ("Patient".equals(resourceType) == false) {
				throw new InvalidRequestException("Invalid resource type for parameter 'subject': " + resourceType);
			}

		}

		if (theSubject != null) {

			String subjectId = theSubject.getIdPart();
			logger.debug("Subject Id: " + subjectId);

			if (theDate == null) {
				throw new InvalidRequestException("Not AuthoredOn Parameter");
			}

			Date date = theDate.getValue();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			String orderDate = format.format(date);

			logger.debug("Order Date: " + orderDate);

			OauthClient oauthClient = new OauthClient();

			String API_URL = "/api/v1/mdp/order/patient-drug-order";

			// Set Parameters
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(SERVER_URL + API_URL)
					.queryParam("ptno", subjectId)							
					.queryParam("codvCd", "I")
					.queryParam("ordrYmd", orderDate);
			
			String responseString = oauthClient.getData(builder.toUriString());

			// Test by Static String Value
//			String responseString = "{\r\n" + 
//					"  \"mdpOrdrInfoDrugOutDVOList\": [\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"CANDE8\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Atacand tab 8mg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"M\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"ISDN20\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Isomack Retard cap 20mg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"C\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"M\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"BISOP2\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Concor tab 2.5mg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"M\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"CHOLN\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Gliatilin SC 400mg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"C\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"M\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"ATORV10\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Lipitor tab 10mg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"M\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"LTXN1H\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Synthyroxine tab 100mcg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"AM1\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"RENAL\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Renalmin tab\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"M\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"FURO40\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Lasix tab 40mg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 2,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"M\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"LACTO\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Lactobay EC cap\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"C\",\r\n" + 
//					"      \"ntm\": \"  3\",\r\n" + 
//					"      \"drusCd\": \"3P\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"CPHE\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Peniramine tab 2mg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  2\",\r\n" + 
//					"      \"drusCd\": \"MHS\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"EBAS\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Ebastel tab 10mg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 0.5,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"HS\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"LTXN50\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】Synthyroxine tab 50mcg\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"T\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"AM1\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"X5DW5HB\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】5% DW inj 500ml (Bag)\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"BAG\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"INF\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    },\r\n" + 
//					"    {\r\n" + 
//					"      \"ordrCd\": \"XMVH\",\r\n" + 
//					"      \"ordrNm\": \"Intra Op.】MVH inj 5ml\",\r\n" + 
//					"      \"orfrNm\": \"추가(약)\",\r\n" + 
//					"      \"afiDrugAdqtStr\": 1,\r\n" + 
//					"      \"mdtnUnitCtn\": \"V\",\r\n" + 
//					"      \"ntm\": \"  1\",\r\n" + 
//					"      \"drusCd\": \"INF\",\r\n" + 
//					"      \"ddcn\": null,\r\n" + 
//					"      \"detlCdNm\": null,\r\n" + 
//					"      \"drugOrdrPrssCd\": \"0\"\r\n" + 
//					"    }\r\n" + 
//					"  ]\r\n" + 
//					"}";

			MdpOrderDrug mdpOrderDrug = new Gson().fromJson(responseString, MdpOrderDrug.class);

			retVal = mdpOrderDrug.generateMedicationResourceBundle(subjectId, date);

		}

		return retVal;

	}

}
