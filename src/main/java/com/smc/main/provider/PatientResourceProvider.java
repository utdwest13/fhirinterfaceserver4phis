package com.smc.main.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jaxrs.server.AbstractJaxRsResourceProvider;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Patient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.smc.main.model.ApbPatient;
import com.smc.main.model.MdpDprtMdl;
import com.smc.main.phis.OauthClient;

@Component
public class PatientResourceProvider extends AbstractJaxRsResourceProvider<Patient> {

	private static final Logger logger = LoggerFactory.getLogger(PatientResourceProvider.class);
	
	private static Long counter = 1L;
	
	private static String SERVER_URL = "http://210.89.179.113:8280/ehr-mbl/1.0";

	private static final ConcurrentHashMap<String, Patient> patients = new ConcurrentHashMap<>();

	static {
		patients.put(String.valueOf(counter), createPatient("Van Houte"));
		patients.put(String.valueOf(counter), createPatient("Agnew"));
		for (int i = 0; i < 20; i++) {
			patients.put(String.valueOf(counter), createPatient("Random Patient " + counter));
		}
	}

	public PatientResourceProvider(FhirContext fhirContext) {
		super(fhirContext);
	}

	@Read
	public Patient find(@IdParam final IdType theId) {
//		if (patients.containsKey(theId.getIdPart())) {
//			return patients.get(theId.getIdPart());
//		} else {
//			throw new ResourceNotFoundException(theId);
//		}
		String patientId = theId.getIdPart();
		
		logger.debug("Patient Id: "+patientId);
						
		OauthClient oauthClient = new OauthClient();
		
		String API_URL = "/api/v1/apb/patient/" + patientId + "/patient-info";
		
		logger.debug(SERVER_URL+API_URL);
		
		String reponseString = oauthClient.getData(SERVER_URL, API_URL);
		
		
		//Set Data for Test
//		String reponseString = "{" + 
//				"  \"ptno\": \"10021900\", " + 
//				"  \"ptntNm\": \"배남선\", " + 
//				"  \"gendCd\": \"F\", " + 
//				"  \"ageVl\": 70 " + 
//				"}";
				
        ApbPatient apbPatient = new Gson().fromJson(reponseString, ApbPatient.class);
        Patient patient = apbPatient.generatePatientResource();
        
        String logicalId = "10021900";
    	String versionId = "1"; // optional
    	patient.setId(new IdType("Patient", logicalId, versionId));
		
		return patient;
	}

	@Create
	public MethodOutcome createPatient(@ResourceParam Patient patient) {

		patient.setId(createId(counter, 1L));
		patients.put(String.valueOf(counter), patient);

		return new MethodOutcome(patient.getIdElement());
	}

	@Search
	public List<Patient> getPatient(
			@OptionalParam(name = Patient.SP_IDENTIFIER) TokenParam theId,
			@OptionalParam(name = Patient.SP_FAMILY) StringParam family
			) {

		List<Patient> retVal = new ArrayList<Patient>();
		
		String patientId = theId.getValue();
		
		OauthClient oauthClient = new OauthClient();
		
		String API_URL = "/api/v1/apb/patient/" + patientId + "/patient-info";
		
		String reponseString = oauthClient.getData(SERVER_URL, API_URL);
		
		//Set Data for Test
//		String reponseString = "{" + 
//				"  \"ptno\": \"10021900\", " + 
//				"  \"ptntNm\": \"배남선\", " + 
//				"  \"gendCd\": \"F\", " + 
//				"  \"ageVl\": 70 " + 
//				"}";
				
        ApbPatient apbPatient = new Gson().fromJson(reponseString, ApbPatient.class);
        Patient patient = apbPatient.generatePatientResource();
        
        String logicalId = patientId;
//        String logicalId = "10021900";
        
    	String versionId = "1"; // optional
    	patient.setId(new IdType("Patient", logicalId, versionId));
        
		
		retVal.add(patient);
		return retVal;

	}

//    @Search
//    public List<Patient> getPatients(@OptionalParam(name=Patient.SP_FAMILY) StringParam theFamily) {
//    	
//    	Patient patient = new Patient();
//    	patient.addIdentifier().setSystem("urn:mrns").setValue("12345");
//    	patient.addName().setFamily("Smith").addGiven("Tester").addGiven("Q");
//    	
//    	String logicalId = "4325";
//    	String versionId = "2"; // optional
//    	patient.setId(new IdType("Patient", logicalId, versionId));
//    	
//    	List<Patient> retVal = new ArrayList<Patient>();
//    	retVal.add(patient);
//    	return retVal;
//    	
//    }

//    @Search
//    public List<Patient> searchByLastName(@RequiredParam(name=Patient.SP_FAMILY) StringParam theFamily) {
//    	
//    	Patient patient = new Patient();
//    	patient.addIdentifier().setSystem("urn:mrns").setValue("12345");
//    	patient.addName().setFamily("Smith").addGiven("Tester").addGiven("Q");
//    	
//    	String logicalId = "4325";
//    	String versionId = "2"; // optional
//    	patient.setId(new IdType("Patient", logicalId, versionId));
//    	
//    	List<Patient> retVal = new ArrayList<Patient>();
//    	retVal.add(patient);
//    	return retVal;
//    	
//    }

	@Override
	public Class<Patient> getResourceType() {
		return Patient.class;
	}

	private static IdType createId(final Long id, final Long theVersionId) {
		return new IdType("Patient", "" + id, "" + theVersionId);
	}

	private static Patient createPatient(final String name) {
		final Patient patient = new Patient();
		patient.getName().add(new HumanName().setFamily(name));
		patient.setId(createId(counter, 1L));
		counter++;
		return patient;
	}
	


}
