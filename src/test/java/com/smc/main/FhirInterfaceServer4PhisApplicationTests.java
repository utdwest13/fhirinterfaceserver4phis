package com.smc.main;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.smc.main.model.MdpDprtMdl;
import com.smc.main.phis.OauthClient;

import org.junit.Assert;

@SpringBootTest
public class FhirInterfaceServer4PhisApplicationTests {

	
//	@Test
//    public void test(){
//		OauthClient client = new OauthClient();
//        accessToken = client.getOauth2Token("fhiradmin", "7418bkyI5296");
//        System.out.println(accessToken);
//    }
//	
//	@Test
//    public void getOAuth2TokenTest(){
//        Assert.assertNotNull(accessToken);
//    }

    @Test
    public void getTestWithAccessToken(){
    	
		OauthClient client = new OauthClient();
		String accessToken = client.getOauth2Token("fhiradmin", "7418bkyI5296");
    	
        final String SERVER_URL = "http://210.89.179.113:8280/ehr-mbl/1.0/api/v1";
        final String API_URL = "/mdp/department/medical-dept-list";

        RestTemplate restTemplate = new RestTemplate();
        
        System.out.println("Access Token: " + accessToken);
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + accessToken);
        headers.add("Accept", "*/*");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
        
        ResponseEntity<String> responseEntity = restTemplate.exchange(SERVER_URL + API_URL, HttpMethod.GET, request, String.class);
        
        String reponseString = (String) responseEntity.getBody();
        System.out.println("Original Output : " + reponseString + "\n");
        
        MdpDprtMdl mdpDprtMdl = new Gson().fromJson(reponseString, MdpDprtMdl.class);
        String jsonOut = new Gson().toJson(mdpDprtMdl);
        System.out.println("JSON Output : " + jsonOut);
        

//        Assert.assertEquals("private", responseEntity.getBody());
    }

}
